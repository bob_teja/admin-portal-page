(function($){
  $(function(){
    $('.sidenav').sidenav();
    $('.parallax').parallax();
    $('.datepicker').datepicker();
    $('.fixed-action-btn').floatingActionButton();
    $('select').formSelect();
    $('.modal').modal();
    $('.tooltipped').tooltip();
    $('.datepicker').datepicker();
  }); // end of document ready
  $('.modal').modal();
})(jQuery); // end of jQuery name space
