const searchState = {
    startDate : null,
    endDate : null,
    status : "All",
    name : "NONE",
    passoutYear:"NONE",
    queue : new Set()
};

export default searchState;