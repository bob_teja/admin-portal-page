import PAGINATION_ACTION from "../actions/paginationActionTypes"
import paginationState from "../store/paginationState";

export default function (state = paginationState, action) {
    switch (action.type) {
        case PAGINATION_ACTION.INCREMENT_INDEX:
            {
                return {
                    ...state,
                    index : action.payload
                }
            }
        case PAGINATION_ACTION.DECREMENT_INDEX:
            {
                return {
                    ...state,
                    index : action.payload
                }
            }
        case PAGINATION_ACTION.SET_TO_ONE_INDEX:
            {
                // console.log("Set to one")
                return {
                    ...state,
                    index: action.payload
                }
            }
        // case PAGINATION_ACTION.SET_TO_MAX_INDEX:
        //     {
        //         console.log("Set to Max")
        //         return {
        //             ...state,
        //             index: 20
        //         }
        //     }
        case PAGINATION_ACTION.SET_NUMBER_OF_ITEMS:
            {
                // console.log("Set number of items")
                return {
                    ...state,
                    numberOfItems: action.payload
                }
            }
        default:
            return state;
    }
}