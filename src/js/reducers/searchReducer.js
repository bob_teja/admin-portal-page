import SEARCH_ACTION from "../actions/searchActionsTypes"
import searchState from "../store/searchState";

export default function(state = searchState,action) {
    switch (action.type) {
        case SEARCH_ACTION.SET_START_DATE : {
            // console.log("Start date set")
            return {
                ...state,
                startDate : action.payload
            }
        }

        case SEARCH_ACTION.SET_PASSOUT_YEAR : {
            return {
                ...state,
                passoutYear : action.payload
            }
        }

        case SEARCH_ACTION.SET_END_DATE : {
            // console.log("End date set")
            return {
                ...state,
                endDate : action.payload
            }
        }
        case SEARCH_ACTION.SET_STATUS : {
            // console.log(action.payload)
            return {
                ...state,
                status : action.payload
            }
        }

        case SEARCH_ACTION.ADD_TO_QUEUE : {
            return {
                ...state,
                queue : action.payload
            }
        }

        case SEARCH_ACTION.REMOVE_FROM_QUEUE : {
            return {
                ...state,
                queue : action.payload
            }
        }

        case SEARCH_ACTION.CLEAR_QUEUE : {
            return {
                ...state,
                queue : action.payload
            }
        }
        

        default: return state;
    }
}


// for(let i = 0; i < studentDetailsArray.length; i++) {
//     if(studentDetailsArray[i].id === action.payload) {
//         studentDetailsArray.splice(i, 1); 
//     }
// }


// var studentDetailsArray = state.studentDetails;
//             for(let i = 0; i < studentDetailsArray.length; i++) {
//                 if(studentDetailsArray[i].id === action.payload) {
//                     studentDetailsArray.splice(i, 1); 
//                 }
//             }
//             return {
//                studentDetails : studentDetailsArray 
//             }