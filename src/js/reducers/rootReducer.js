import {combineReducers} from 'redux';
import StudentDetailsReducer from  "./studentDetailsReducer"
import SearchReducer from "./searchReducer"
import PaginationReducer from './paginationReducer';

const allReducers = combineReducers({
    studentDetails: StudentDetailsReducer,
    search: SearchReducer,
    pagination:PaginationReducer
});

export default allReducers;