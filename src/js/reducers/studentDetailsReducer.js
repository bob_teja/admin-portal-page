import STUDENT_DETAILS_ACTION from "../actions/studentDetaislActionTypes"
import studentDetailState from "../store/studentDetailState";

export default function(state = studentDetailState,action) {
    switch (action.type) {
        case STUDENT_DETAILS_ACTION.ADD_STUDENT_DETAILS : {
            return {
                studentDetails: action.payload
            }
        }

        case STUDENT_DETAILS_ACTION.DELETE_STUDENT_DETAILS : {
            console.log("Delete Student details")
            console.log(state)
            console.log(action.payload)
            var studentDetailsArray = state.studentDetails;
            for(let i = 0; i < studentDetailsArray.length; i++) {
                if(studentDetailsArray[i].id === action.payload) {
                    studentDetailsArray.splice(i, 1); 
                }
            }
            return {
               studentDetails : studentDetailsArray 
            }
        }

        default: return state;
    }
}