import PAGINATION_ACTION from "./paginationActionTypes"

export const incrementIndex = () => (dispatch,getState) => {
    let index = getState().pagination.index + 1;
    dispatch({
        type: PAGINATION_ACTION.INCREMENT_INDEX,
        payload: index,
    });
}

export const decrementIndex =  () => (dispatch,getState) => {
    let index = getState().pagination.index;
    if(index !== 1) {
        index --;
    }
    dispatch({
        type: PAGINATION_ACTION.INCREMENT_INDEX,
        payload: index,
    });
}

export const setToOneIndex = () => {
    return {
        type: PAGINATION_ACTION.SET_TO_ONE_INDEX,
        payload: 1
    }
}

// export const setToMaxIndex = () => {
//     return {
//         type: PAGINATION_ACTION.SET_TO_MAX_INDEX,
//         payload: null
//     }
// }

export const setNumberOfItems = (numberOfItems) => {
    return {
        type: PAGINATION_ACTION.SET_NUMBER_OF_ITEMS,
        payload: parseInt(numberOfItems)
    }
}
