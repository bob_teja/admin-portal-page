import SEARCH_ACTION from "./searchActionsTypes"

export const setStartDate = (startDate) => {
    return {
        type: SEARCH_ACTION.SET_START_DATE,
        payload: startDate
    }
}

export const setEndDate = (endDate) => {
    return {
        type: SEARCH_ACTION.SET_END_DATE,
        payload: endDate
    }
}

export const setStatus = (status) => {
    return {
        type: SEARCH_ACTION.SET_STATUS,
        payload: status
    }
}

export const addToQueue = (id) => (dispatch, getState) => {
    const queue = getState().search.queue;
    queue.add(id)
    dispatch({
        type: SEARCH_ACTION.ADD_TO_QUEUE,
        payload: queue,
    });
}

export const removeFromQueue = (id) => (dispatch, getState) => {
    const queue = getState().search.queue;
    queue.delete(id);
    dispatch({
        type: SEARCH_ACTION.REMOVE_FROM_QUEUE,
        payload: queue,
    });

}

export const clearQueue = () => {
    return {
        type: SEARCH_ACTION.CLEAR_QUEUE,
        payload: new Set()
    }
}

export const setPassoutYear = (year) => {
    return {
        type: SEARCH_ACTION.SET_PASSOUT_YEAR,
        payload: year
    }
}