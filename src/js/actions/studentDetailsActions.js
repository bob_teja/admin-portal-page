import STUDENT_DETAILS_ACTION from "./studentDetaislActionTypes"
import axios from 'axios';
import CONST from "../utils/constants"

export const setStudentDetails = (studentDetails) => {
    console.log(studentDetails)
    return {
        type: STUDENT_DETAILS_ACTION.ADD_STUDENT_DETAILS,
        payload: studentDetails
    }
};

export const deleteStudent = (id,dispatch,getState) => {
    // var studentDetailsArray = getState().studentDetails.studentDetails;
    // console.log(studentDetailsArray)
    // for(let i = 0; i < studentDetailsArray.length; i++) {
    //     if(studentDetailsArray[i].id === id) {
    //         console.log("found")
    //         console.log(studentDetailsArray[i]);
    //         studentDetailsArray.splice(i, 1); 
    //     }
    // }
    // console.log("printing")
    // console.log(studentDetailsArray)
    // dispatch({
    //     type: STUDENT_DETAILS_ACTION.DELETE_STUDENT_DETAILS,
    //     payload: studentDetailsArray,
    // });
    console.log("Dispatched")
}

export const getStudentDetailsClicked = () => (dispatch, getState) => {
    const {
        pagination,
        search
    } = getState();
    //let studentDetailsFetchAPI = CONST.URL + "admin/secure/studentInfo?name=NONE&status=" + status;
    let studentDetailsFetchAPI = CONST.URL + "admin/secure/studentInfo?name=" + search.name + "&status=" + search.status + "&year="+search.passoutYear+"&percentage=50&page=" + (pagination.index - 1) + "&size=" + pagination.numberOfItems + "";
    // console.log(studentDetailsFetchAPI)
    axios.get(studentDetailsFetchAPI)
        .then(responseData => responseData.data)
        .then(data => {
            if (data == null) {
                console.log("hi")
                return;
            }
            dispatch({
                type: STUDENT_DETAILS_ACTION.ADD_STUDENT_DETAILS,
                payload: data,
            });
        });
}

export const approveStudentAPICall = () => (dispatch, getState) => {
    var queue = Array.from(getState().search.queue)
    var data = {
        "id": Array.from(queue)
    }
    let approveStudentFetchAPI = CONST.URL + "admin/secure/approveStudent";
    // console.log(approveStudentFetchAPI)
    axios.post(approveStudentFetchAPI, data)
        .then(responseData => responseData.data)
        .then(data => {
            if (data == null) {
                return;
            }
            if (data) {
                for (let i = 0; i < queue.length; i++) {
                    console.log("delete the data baby");
                    dispatch({
                        type: STUDENT_DETAILS_ACTION.DELETE_STUDENT_DETAILS,
                        payload: queue[i],
                    });
                    //deleteStudent(queue[i], dispatch, getState);
                }
            }
        });
}

export const rejectStudentAPICall = () => (dispatch, getState) => {
    var queue = Array.from(getState().search.queue)
    var data = {
        "id": queue
    }
    let rejectStudentFetchAPI = CONST.URL + "admin/secure/rejectStudent";
    // console.log(rejectStudentFetchAPI)
    axios.post(rejectStudentFetchAPI, data)
        .then(responseData => responseData.data)
        .then(data => {
            if (data == null) {
                return;
            }
            if (data) {
                for (let i = 0; i < queue.length; i++) {
                    console.log("delete the data baby");
                    dispatch({
                        type: STUDENT_DETAILS_ACTION.DELETE_STUDENT_DETAILS,
                        payload: queue[i],
                    });
                }
            }
        });
}