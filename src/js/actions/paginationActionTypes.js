const PAGINATION_ACTION = {
    INCREMENT_INDEX: "INCREMENT_INDEX",
    DECREMENT_INDEX: "DECREMENT_INDEX",
    SET_TO_ONE_INDEX: "SET_TO_ZERO_INDEX",
    SET_TO_MAX_INDEX: "SET_TO_MAX_INDEX",
    SET_NUMBER_OF_ITEMS: "SET_NUMBER_OF_ITEMS"
}

export default PAGINATION_ACTION;