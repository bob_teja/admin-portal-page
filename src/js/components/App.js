import React, { Component } from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from "redux";


import { getStudentDetailsClicked, approveStudentAPICall, rejectStudentAPICall} from "../actions/studentDetailsActions"
import {setStartDate, setEndDate, setStatus, clearQueue, setPassoutYear} from "../actions/searchActions"
import {incrementIndex, decrementIndex, setNumberOfItems, setToOneIndex} from "../actions/paginationActions"
import StudentListItem from './StudentListItem';

class App extends Component {
  
  constructor(props) {
    super(props);
    this.displayDate = this.displayDate.bind(this);
    this.approveStudents = this.approveStudents.bind(this);
    this.rejectStudents = this.rejectStudents.bind(this);
  }

  approveStudents () {
      console.log("approve clicked")
      this.props.approveStudentAPICall();
      this.props.clearQueue();
      this.props.setToOneIndex();
      this.props.getStudentDetailsClicked();
  }

  rejectStudents () {
      console.log("reject clicked")
      this.props.rejectStudentAPICall();
      this.props.clearQueue();
      this.props.setToOneIndex();
      this.props.getStudentDetailsClicked();
  }

  displayDate  (event)  {
    event.preventDefault();
    // var startDate = document.getElementById("startDate").value;
    // var endDate = document.getElementById("endDate").value;
    var status = document.getElementById("status").value;
    var passoutYear = document.getElementById("passoutYear").value;

    // //Validations
    // if(startDate.length===0){alert("Please Select Start date");return;}
    // if(endDate.length===0){alert("Please Select End  date");return;}
    // if(+new Date(startDate).getDate() > +new Date(endDate).getDate()){alert("Improper start date and end date selected");return;}
  
    
    // this.props.setStartDate(startDate);
    // this.props.setEndDate(endDate);
    this.props.setStatus(status);
    this.props.setPassoutYear(passoutYear);
    this.props.setToOneIndex();
    this.props.getStudentDetailsClicked();

  }
  render() {
    const {studentDetails} = this.props.studentDetailsReducer;
    return (
      <div className="container">
          <div className="section">
      
              <div className="row">
                  <form onSubmit={this.displayDate}>
      
      
                      <div className="input-field col s3">
                          <input type="text" id="name"/>
                          <label htmlFor="name">College Name</label>
                      </div>

                      <div className="input-field col s3">
                          <select id="passoutYear">
                              <option value="2017">2017</option>
                              <option value="2018">2018</option>
                              <option value="2019">2019</option>
                          </select>
                      </div>

                      

                      {/* <div className="input-field col s3">
                          <input type="text" id="startDate" className="datepicker" />
                          <label htmlFor="startDate">Start Date</label>
                      </div>
      
                      <div className="input-field col s3">
                          <input type="text" id="endDate" className="datepicker" />
                          <label htmlFor="endDate">End Date</label>
                      </div> */}
      
      
                      <div className="input-field col s3">
                          <select id="status">
                              <option value="HOLD">Hold</option>
                              <option value="APPROVED">Approved</option>
                              <option value="REJECTED">Rejected</option>
                          </select>
                      </div>

                      <div className="input-field col s3">
                            <select onChange={(event)=>{this.props.setNumberOfItems(event.target.value);}}>
                                <option value="1">1</option>
                                <option value="10">10</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                      </div>

                      <div className="input-field col s3">
                          <button className="btn waves-effect waves-light" type="submit" name="action">Get Details
                              <i className="material-icons right">send</i>
                          </button>
                      </div>
                      
                      {/* <div className="input-field col s3">
                          <button className="btn waves-effect waves-light" type="button" name="action" onClick={this.approveStudents}>Approve Selected
                              <i className="material-icons right">done</i>
                          </button>
                      </div>

                      <div className="input-field col s3">
                          <button className="btn waves-effect waves-light" type="button" name="action" onClick={this.rejectStudents}>Reject Selected
                              <i className="material-icons right">close</i>
                          </button>
                      </div> */}
                      <button  type="button"  onClick={this.approveStudents}>Approve Selected
                          </button> 
                        <button  type="button"  onClick={this.rejectStudents}>Reject Selected
                          </button> 



                      {/* <button onClick={this.approveStudents}>Approve Selected</button>
                      <button onClick={this.rejectStudents}>Reject Selected</button> */}

                      
      
                  </form>
              </div>
      
              <div className="row center">
                  <div className="col s12 m8 offset-m2">
                      <h4 className="header light">Student Details</h4>
                  </div>
              </div>
              {
                (studentDetails !== null)
                ? (studentDetails != "" )
                    ?studentDetails.map((eachStudent)=>
                        <StudentListItem key={Math.floor(Math.random()*1000000)} student={eachStudent} />)
                    :<h5 className="header light center">No Data</h5>
                : <h5 className="header light center">Submit the form to load data</h5>
              }
              
          </div>
          <div className="row">
            <div className="input-field col s3">
                <button className="btn waves-effect waves-light" type="button" name="action" onClick={()=>{this.props.decrementIndex();this.props.getStudentDetailsClicked()}}>
                    <i className="material-icons right">arrow_back_ios</i>
                </button>
            </div>
            {/* <div>
            {this.props.paginationReducer.index}
            </div>
            <div className="col s12 m8 offset-m2">
            </div>  */}
            <div className="col s3" >
                {this.props.paginationReducer.index}
            </div>
            <div className="input-field col s3">
                <button className="btn waves-effect waves-light" type="button" name="action" onClick={()=>{this.props.incrementIndex();this.props.getStudentDetailsClicked()}}>
                    <i className="material-icons right">arrow_forward_ios</i>
                </button>
            </div>
          </div> 
          {/* <button onClick={()=>{this.props.setToOneIndex();this.props.getStudentDetailsClicked()}}>First</button> 
           
          <button onClick={()=>{this.props.incrementIndex();this.props.getStudentDetailsClicked()}}>Last</button> */}
          
          
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    studentDetailsReducer: state.studentDetails,
    paginationReducer:state.pagination
  };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        setStartDate: setStartDate,
        setEndDate: setEndDate,
        setStatus: setStatus,
        getStudentDetailsClicked: getStudentDetailsClicked,
        incrementIndex: incrementIndex,
        decrementIndex: decrementIndex,
        setNumberOfItems: setNumberOfItems,
        approveStudentAPICall : approveStudentAPICall,
        rejectStudentAPICall:rejectStudentAPICall,
        clearQueue:clearQueue,
        setToOneIndex:setToOneIndex,
        setPassoutYear:setPassoutYear
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(App);

