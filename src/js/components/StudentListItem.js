import React, { Component } from 'react';
import {connect} from 'react-redux';
import {rejectStudentAPICall,approveStudentAPICall, getStudentDetailsClicked} from '../actions/studentDetailsActions'
import {addToQueue,removeFromQueue,clearQueue} from "../actions/searchActions"
import {bindActionCreators} from "redux";


import CONST from "../utils/constants" 
//import '../css/StudentListItem.css'

class StudentListItem extends Component {

    constructor(props) {
        super(props);
        this.state = {
            imgLink : null
        }
        this.viewPhoto = this.viewPhoto.bind(this);
        this.viewResume = this.viewResume.bind(this);
        this.approveStudent = this.approveStudent.bind(this);
        this.rejectStudent = this.rejectStudent.bind(this);
        this.handleChangeChk = this.handleChangeChk.bind(this);
    }

    handleChangeChk(event) {
        var isChecked = event.target.checked;
        var id = this.props.student.id;
        if(isChecked) {
            this.props.addToQueue(id);
        } else {
            this.props.removeFromQueue(id);
        }
    }

    viewResume (event)  {
        event.preventDefault();
        console.log("View Resume "+ this.props.student.firstName);
        console.log("ID "+ this.props.student.id)
    }

    viewPhoto (event)  {
        event.preventDefault();
        const imgLink = CONST.URL+"user/secure/file?id="+this.props.student.id+"&type=image";
        console.log(imgLink);
        var image = new Image();
        image.src = imgLink;
        var w = window.open("");

        //Opens image in new tab
        w.document.write(image.outerHTML);
    }

    approveStudent(event)  {
        event.preventDefault();
        // console.log("Approve Student "+ this.props.student.firstName);
        // console.log("ID "+ this.props.student.id)
        this.props.addToQueue(this.props.student.id);
        this.props.approveStudentAPICall();
        this.props.clearQueue();
        //this.props.getStudentDetailsClicked();

    }

    rejectStudent (event)  {
        event.preventDefault();
        // console.log("Reject Student "+ this.props.student.firstName);
        // console.log("ID "+ this.props.student.id);
        this.props.addToQueue(this.props.student.id);
        this.props.rejectStudentAPICall();
        this.props.clearQueue();
        //this.props.getStudentDetailsClicked();
    }
    render() {
        const {student} = this.props;
        return (
            <div className="row card z-depth-3 valign-wrapper" style={{margin: "10px 0", padding: "10px"}}>

                <div className="col s1 m1">           
                    <label>
                        <input type="checkbox" onChange={this.handleChangeChk}/>
                        <span></span>
                    </label>
                </div>    

                <div className="col s12 m2">
                    <span>{student.firstName}</span>
                </div>

                <div className="col s12 m2">
                    <span>{student.lastName}</span>
                </div>

                <div className="col s12 m2">
                    <span>{student.dateOfBirth}</span>
                </div>
                <div className="col s12 m3">
                    <span>{student.name}</span>
                </div>
                <div className="col s12 m2">
                    <a className="right btn-small waves-effect waves-light btn" onClick={this.viewPhoto}>View Govt.ID</a>
                </div>

                <div className="col s12 m1">
                    <a className="right btn-floating btn-small waves-effect waves-light green" onClick={this.approveStudent}>
                        <i className="material-icons">check</i>
                    </a>
                </div>

                <div className="col s12 m1">
                    <a className="right btn-floating btn-small waves-effect waves-light red" onClick={this.rejectStudent}>
                        <i className="material-icons">close</i>
                    </a>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
      studentDetailsReducer: state.studentDetails
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        rejectStudentAPICall : rejectStudentAPICall,
        approveStudentAPICall : approveStudentAPICall,
        addToQueue : addToQueue,
        removeFromQueue : removeFromQueue,
        clearQueue:clearQueue,
        getStudentDetailsClicked : getStudentDetailsClicked
    }, dispatch);
  }
  
export default connect(mapStateToProps,mapDispatchToProps)(StudentListItem);